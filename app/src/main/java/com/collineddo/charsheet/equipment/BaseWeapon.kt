package com.collineddo.charsheet.equipment

import com.collineddo.charsheet.Globals

/**
 * Created by Collin on 6/9/2018.
 */
class BaseWeapon(isMelee: Boolean,
                 isReachWeapon: Boolean,
                 isThrownWeapon: Boolean,
                 size: Globals.SIZE,
                 weaponType: Globals.WEAPON_TYPE,
                 weaponSubtype: Globals.WEAPON_SUBTYPE,
                 weaponDamageTypePrimary: Globals.WEAPON_DAMAGE_TYPE,
                 weaponDamageTypeSecondary: Globals.WEAPON_DAMAGE_TYPE,
                 minRange: Int,
                 maxRange: Int,
                 rangeIncrement: Int,
                 weight: Int,
                 costGP: Int) {

    //Set at top level
    var isMelee: Boolean = false
    var isReachWeapon: Boolean = false
    var isThrownWeapon: Boolean = false
    var size: Globals.SIZE
    var weaponType: Globals.WEAPON_TYPE
    var weaponSubtype: Globals.WEAPON_SUBTYPE
    var weaponDamageTypePrimary: Globals.WEAPON_DAMAGE_TYPE
    var weaponDamageTypeSecondary: Globals.WEAPON_DAMAGE_TYPE
    private val minRange: Int
    private val maxRange: Int
    var rangeIncrement: Int = 0
    var weight: Int = 0
    var costGP: Int = 0

    init {
        this.isMelee = isMelee
        this.isReachWeapon = isReachWeapon
        this.isThrownWeapon = isThrownWeapon
        this.size = size
        this.weaponType = weaponType
        this.weaponSubtype = weaponSubtype
        this.weaponDamageTypePrimary = weaponDamageTypePrimary
        this.weaponDamageTypeSecondary = weaponDamageTypeSecondary
        this.minRange = minRange
        this.maxRange = maxRange
        this.rangeIncrement = rangeIncrement
        this.weight = weight
        this.costGP = costGP
    }

    fun getMaxRangeIncrement(): Int {
        return if (isThrownWeapon) {
            rangeIncrement * 5
        } else if (!isMelee) {
            rangeIncrement * 10
        } else {
            10
        }
    }

    fun getWeaponRange(characterSize: Globals.SIZE): IntArray {
        val range = IntArray(2)
        if (isReachWeapon) {
            if (characterSize === Globals.SIZE.SMALL) {
                range[0] = 10
                range[1] = 10
            } else {
                range[0] = 15
                range[1] = 20
            }
        } else {
            range[0] = minRange
            range[1] = maxRange
        }
        return range
    }
}