package com.collineddo.charsheet

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

/**
 * Created by Collin on 6/9/2018.
 */
class ActivityMain : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }
}