package com.collineddo.charsheet

/**
 * Created by Collin on 6/9/2018.
 */
class ModelBaseStats(intelligence: Int, strength: Int, constitution: Int, wisdom: Int, dexterity: Int, charisma: Int) {

    var intelligence: Int = 0
    var strength: Int = 0
    var constitution: Int = 0
    var wisdom: Int = 0
    var dexterity: Int = 0
    var charisma: Int = 0

    init {
        this.intelligence = intelligence
        this.strength = strength
        this.constitution = constitution
        this.wisdom = wisdom
        this.dexterity = dexterity
        this.charisma = charisma
    }

    fun getModifier(baseStat: Globals.BASE_STAT): Int {
        return when (baseStat) {
            Globals.BASE_STAT.INTELLIGENCE -> getModifierForTotal(intelligence)
            Globals.BASE_STAT.STRENGTH ->  getModifierForTotal(strength)
            Globals.BASE_STAT.CONSTITUTION ->  getModifierForTotal(constitution)
            Globals.BASE_STAT.WISDOM ->  getModifierForTotal(wisdom)
            Globals.BASE_STAT.DEXTERITY ->  getModifierForTotal(dexterity)
            Globals.BASE_STAT.CHARISMA -> getModifierForTotal(charisma)
        }
    }

    private fun getModifierForTotal(total: Int): Int {
        val modifier = (total - 10) / 2
        return if (modifier < 0) 0 else modifier
    }
}