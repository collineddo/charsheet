package com.collineddo.charsheet

/**
 * Created by Collin on 6/9/2018.
 */
class Globals {

    enum class BASE_STAT {
        STRENGTH,
        DEXTERITY,
        CONSTITUTION,
        INTELLIGENCE,
        WISDOM,
        CHARISMA
    }

    enum class CLASS {
        BARBARIAN,
        BARD,
        CLERIC,
        DRUID,
        FIGHTER,
        MONK,
        PALADIN,
        RANGER,
        ROGUE,
        SORCERER,
        WIZARD
    }

    enum class ALIGNMENT {
        GOOD,
        NEUTRAL,
        EVIL,
        LAWFUL,
        CHAOTIC
    }

    enum class RACE {
        HUMAN,
        HALFLING,
        GNOME,
        HALF_ORC,
        DWARF,
        HALF_ELF,
        ELF;

        val size: SIZE
            get() {
                var size = SIZE.MEDIUM
                when (this) {
                    HUMAN -> size = SIZE.MEDIUM
                    DWARF -> size = SIZE.MEDIUM
                    ELF -> size = SIZE.MEDIUM
                    GNOME -> size = SIZE.SMALL
                    HALF_ELF -> size = SIZE.MEDIUM
                    HALF_ORC -> size = SIZE.MEDIUM
                    HALFLING -> size = SIZE.SMALL
                }
                return size
            }
    }

    enum class SIZE {
        COLOSSAL,
        GARGANTUAN,
        HUGE,
        LARGE,
        MEDIUM,
        SMALL,
        TINY,
        DIMINUTIVE,
        FINE;

        val grappleSizeBonus: Int
            get() {
                var bonus = 0
                when (this) {
                    COLOSSAL -> bonus = 16
                    GARGANTUAN -> bonus = 12
                    HUGE -> bonus = 8
                    LARGE -> bonus = 4
                    MEDIUM -> bonus = 0
                    SMALL -> bonus = -4
                    TINY -> bonus = -8
                    DIMINUTIVE -> bonus = -12
                    FINE -> bonus = -16
                }
                return bonus
            }
    }

    enum class DIE {
        D3,
        D4,
        D6,
        D8,
        D10,
        D12,
        D20
    }

    enum class FEAT {
        //////////////////////////////////////////////////
        //GENERAL
        //////////////////////////////////////////////////
        ACROBATIC,
        AGILE,
        ALERTNESS,
        ANIMAL_AFFINITY,
        ARMOR_PROFICIENCY_HEAVY,
        ARMOR_PROFICIENCY_MEDIUM,
        ARMOR_PROFICIENCY_LIGHT,
        ATHLETIC,
        AUGMENT_SUMMONING,
        BLIND_FIGHT,
        COMBAT_CASTING,
        COMBAT_EXPERTISE,
        IMPROVED_DISARM,
        IMPROVED_FEINT,
        IMPROVED_TRIP,
        WHIRLWIND_ATTACK,
        COMBAT_REFLEXES,
        DECEITFUL,
        DEFT_HANDS,
        DILIGENT,
        DODGE,
        MOBILITY,
        SPRING_ATTACK,
        ENDURANCE,
        DIEHARD,
        ESCHEW_MATERIALS,
        EXOTIC_WEAPON_PROFICIENCY,
        EXTRA_TURNING,
        GREAT_FORTITUDE,
        IMPROVED_COUNTERSPELL,
        IMPROVED_CRITICAL,
        IMPROVED_INITIATIVE,
        IMPROVED_TURNING,
        IMPROVED_UNARMED_STRIKE,
        IMPROVED_GRAPPLE,
        DEFLECT_ARROWS,
        SNATCH_ARROWS,
        STUNNING_FIST,
        INVESTIGATOR,
        IRON_WILL,
        LEADERSHIP,
        LIGHTNING_REFLEXES,
        MAGICAL_APTITUDE,
        MARTIAL_WEAPON_PROFICIENCY,
        MOUNTED_COMBAT,
        MOUNTED_ARCHERY,
        RIDE_BY_ATTACK,
        SPIRITED_CHARGE,
        TRAMPLE,
        NATURAL_SPLL,
        NEGOTIATOR,
        NIMBLE_FINGERS,
        PERSUASIVE,
        POINT_BLANK_SHOT,
        FAR_SHOT,
        PRECISE_SHOT,
        RAPID_SHOT,
        MANY_SHOT,
        SHOT_ON_THE_RUN,
        IMPROVED_PRECISE_SHOT,
        POWER_ATTACK,
        CLEAVE,
        GREAT_CLEAVE,
        IMPROVED_BULL_RUSH,
        IMPROVED_OVERRUN,
        IMPROVED_SUNDER,
        QUICK_DRAW,
        RAPID_RELOAD,
        RUN,
        SELF_SUFFICIENT,
        SHIELD_PROFICIENCY,
        IMPROVED_SHIELD_BASH,
        TOWER_SHIELD_PROFICIENCY,
        SIMPLE_WEAPON_PROFICIENCY,
        SKILL_FOCUS,
        SPELL_FOCUS,
        GREATER_SPELL_FOCUS,
        SPELL_MASTERY,
        SPELL_PENETRATION,
        GREATER_SPELL_PENETRATION,
        STEALTHY,
        TOUGHNESS,
        TRACK,
        TWO_WEAPON_FIGHTING,
        TWO_WEAPON_DEFENSE,
        IMPROVED_TWO_WEAPON_FIGHTING,
        GREATER_TWO_WEAPON_FIGHTING,
        WEAPON_FINESS,
        WEAPON_FOCUS,
        WEAPON_SPECIALIZATION,
        GREATER_WEAPON_FOCUS,
        GREATER_WEAPON_SPECIALIZATION,
        //////////////////////////////////////////////////
        //ITEM CREATION
        //////////////////////////////////////////////////
        BREW_POTION,
        CRAFT_MAGIC_ARMS_AND_ARMOR,
        CRAFT_ROD,
        CRAFT_STAFF,
        CRAFT_WAND,
        CRAFT_WONDROUS_ITEM,
        FORGE_RING,
        SCRIBE_SPELL,
        //////////////////////////////////////////////////
        //METAMAGIC
        //////////////////////////////////////////////////
        EMPOWER_SPELL,
        ENLARGE_SPELL,
        EXTEND_SPELL,
        HEIGHTEN_SPELL,
        MAXIMIZE_SPELL,
        QUICKEN_SPELL,
        SILENT_SPELL,
        STILL_SPELL,
        WIDEN_SPELL
    }

    enum class WEAPON_TYPE {
        SIMPLE,
        MARTIAL,
        EXOTIC
    }

    enum class WEAPON_SUBTYPE {
        ONE_HAND,
        TWO_HAND,
        DOUBLE
    }

    enum class WEAPON_DAMAGE_TYPE {
        BLUDGEONING,
        PIERCING,
        SLASHING,
        NONE
    }

}