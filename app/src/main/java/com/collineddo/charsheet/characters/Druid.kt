package com.collineddo.charsheet.characters

import com.collineddo.charsheet.Globals


/**
 * Created by Collin on 6/9/2018.
 */
class Druid(name:String,
            race:Globals.RACE,
            alignment1:Globals.ALIGNMENT,
            alignment2: Globals.ALIGNMENT) : BaseCharacter(name, Globals.CLASS.DRUID, race, arrayOf(alignment1, alignment2)) {


    //TODO: Work into the initial creation that this class can choose from the following for bonus
    //languages: Sylvan
}