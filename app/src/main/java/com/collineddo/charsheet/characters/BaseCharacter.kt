package com.collineddo.charsheet.characters

import com.collineddo.charsheet.Globals
import com.collineddo.charsheet.ModelBaseStats

/**
 * Created by Collin on 6/9/2018.
 */
/**
 * Constructor for initial character creation
 * @param name Name of the character
 * @param klass The characters class
 * @param alignment The characters alignment represented by a 2 member array
 */
open class BaseCharacter internal constructor(name: String, klass: Globals.CLASS, race: Globals.RACE, alignment: Array<Globals.ALIGNMENT>){

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //MEMBERS
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    var mName: String
    var mClass: Globals.CLASS
    var mRace: Globals.RACE
    var mAlignment: Array<Globals.ALIGNMENT>
    var mBaseStats: ModelBaseStats? = null
    var mLevel: Int = 0
    var mBaseAttackBonus: Int = 0//TODO: Set this on construction based on the characters class and any other misc modifiers.

    init {
        mName = name
        mClass = klass
        mRace = race
        mAlignment = alignment
        mLevel = 1
        mBaseAttackBonus = 0//TODO: Get this from your class and wherever else applies modifiers to it
        //TODO Ensure to prompt, at character creation, that if a person chooses to play a human you have them pick 1 extra feat
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //PUBLIC METHODS
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    fun setBaseStats(str: Int, dex: Int, con: Int, intel: Int, wis: Int, cha: Int) {
        var strength = str
        var dexterity = dex
        var constitution = con
        var intelligence = intel
        var charisma = cha
        when (mRace) {
            Globals.RACE.HUMAN -> {
            }
            Globals.RACE.DWARF -> {
                constitution += 2
                charisma = if (charisma < 2) 0 else charisma - 2
            }
            Globals.RACE.ELF -> {
                dexterity += 2
                constitution = if (constitution < 2) 0 else constitution - 2
                constitution += 2
                strength = if (strength < 2) 0 else strength - 2
            }
            Globals.RACE.GNOME -> {
                constitution += 2
                strength = if (strength < 2) 0 else strength - 2
            }
            Globals.RACE.HALF_ELF -> {
            }
            Globals.RACE.HALF_ORC -> {
                strength += 2
                intelligence = if (intelligence < 4) 3 else intelligence - 2
                charisma = if (charisma < 2) 0 else charisma - 2
                dexterity += 2
                strength = if (strength < 2) 0 else strength - 2
            }
            Globals.RACE.HALFLING -> {
                dexterity += 2
                strength = if (strength < 2) 0 else strength - 2
            }
        }
        mBaseStats = ModelBaseStats(intelligence, strength, constitution, wis, dexterity, charisma)
    }

    fun getSpeed(): Int {
        return 30
    }

    fun getInitiativeModifier(): Int {
        return if (mBaseStats == null) {
            0
        } else {
            mBaseStats!!.getModifier(Globals.BASE_STAT.DEXTERITY)
        }
    }

    fun getInitiative(d20Roll: Int): Int {
        //TODO: Make it so this can take 0 and if so, call your global method to generate a random number between 0 and 21
        return getInitiativeModifier() + d20Roll
    }

    fun getBaseAttackBonus(): Int {
        //TODO: Calculate this with everything else that should go with it. Intial total should be set int he constructor, but all the other crap you can add to it
        //should be done here
        return mBaseAttackBonus
    }

    fun getGrappleModifier(): Int {
        //TODO: Your fourth stat here needs to be your misc modifiers which you'll get from your feats most likely
        return if (mBaseStats != null) {
            mBaseAttackBonus + mBaseStats!!.getModifier(Globals.BASE_STAT.STRENGTH) + mRace.size.grappleSizeBonus + 0
        } else {
            0
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //PRIVATE METHODS
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private fun getHitDie(): Globals.DIE {
        //TODO: Figure out why D3 is never used
        //var hitDie: Globals.DIE = Globals.DIE.D3
        return when (mClass) {
            Globals.CLASS.SORCERER -> Globals.DIE.D4
            Globals.CLASS.WIZARD -> Globals.DIE.D4
            Globals.CLASS.BARD -> Globals.DIE.D6
            Globals.CLASS.ROGUE -> Globals.DIE.D6
            Globals.CLASS.CLERIC -> Globals.DIE.D8
            Globals.CLASS.DRUID -> Globals.DIE.D8
            Globals.CLASS.MONK -> Globals.DIE.D8
            Globals.CLASS.RANGER -> Globals.DIE.D8
            Globals.CLASS.FIGHTER -> Globals.DIE.D10
            Globals.CLASS.PALADIN -> Globals.DIE.D10
            Globals.CLASS.BARBARIAN ->Globals.DIE.D12
        }
    }
}