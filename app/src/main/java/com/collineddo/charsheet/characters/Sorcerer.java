package com.collineddo.charsheet.characters;

import com.collineddo.charsheet.Globals;

/**
 * Created by Collin on 1/21/2018.
 */

public class Sorcerer extends BaseCharacter {

    /**
     * Constructor for initial character creation at level 1
     * @param name Name of your character
     * @param alignment1 The first alignment of your character
     * @param alignment2 The second alignment of your character
     */
    public Sorcerer(String name,
                    Globals.RACE race,
                    Globals.ALIGNMENT alignment1,
                    Globals.ALIGNMENT alignment2) {
        super(name, Globals.CLASS.SORCERER, race, new Globals.ALIGNMENT[]{alignment1, alignment2});

    }
}
