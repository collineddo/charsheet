package com.collineddo.charsheet.characters;

import com.collineddo.charsheet.Globals;

/**
 * Created by Collin on 1/21/2018.
 */

public class Wizard extends BaseCharacter {

    //TODO: Work into the initial creation that this class can choose from the following for bonus
    //languages: Draconic

    public Wizard(String name,
                  Globals.RACE race,
                  Globals.ALIGNMENT alignment1,
                  Globals.ALIGNMENT alignment2) {
        super(name, Globals.CLASS.WIZARD, race, new Globals.ALIGNMENT[]{alignment1, alignment2});

    }
}
